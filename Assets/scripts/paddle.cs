﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paddle : MonoBehaviour {


	
	public float paddleSpeed = 1;
	public Vector3 playerPosition;
	void Update () {
		float yPos = gameObject.transform.position.y + (Input.GetAxis("Vertical")) * paddleSpeed;
		playerPosition = new Vector3 (-23, Mathf.Clamp (yPos, -13, 13), 0);
		gameObject.transform.position = playerPosition;
	}
}
